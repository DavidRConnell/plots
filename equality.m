function plotEquality(ax, varargin)
%PLOTEQUALITY add the x = y line to a plot
%   PLOTEQUALITY(AX, VARARGIN) plot the line of equality (x = y) onto axis AX
%   with standard plot arguments VARARGIN.
%
%   See also plot.

    xlim = get(ax, 'XLim');
    ylim = get(ax, 'YLim');
    pmin = min(xlim(1), ylim(1));
    pmax = max(xlim(2), ylim(2));
    plot([pmin pmax], [pmin pmax], varargin{:})
end
