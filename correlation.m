function plotCorrelation(x, y)
    [r2, model, yhat] = stats.regression(x, y);
    plot(x, y, '.');
    hold on
    plot(x, yhat, 'r')
    hold off
    fmt = '%0.2g';
    legend({'$y$', ...
            ['$\hat{y} = ' num2str(model(1), fmt) 'x + ' num2str(model(2), fmt) '$;' ...
            ' $r^2 = ' num2str(r2, fmt) '$']}, ...
           'Interpreter', 'latex', ...
           'Location', 'northwest')
    xticks = get(gca, 'XTick');
    yticks = get(gca, 'YTick');
end
