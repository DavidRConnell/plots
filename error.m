function error(x, y, err, color)
    color
    fill([x; flipud(x)], ...
         [y + (1.96 * err); flipud(y - (1.96 * err))], color, ...
         'FaceAlpha', 0.05)
    hold on
    plot(x, y, 'color', color)
    hold off
end
